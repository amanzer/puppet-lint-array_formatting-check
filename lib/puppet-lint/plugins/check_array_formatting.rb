# frozen_string_literal: true

EXPECTED_NEWLINE_BEFORE_ELEMENT = 'expected newline before element'
EXPECTED_NEWLINE_BEFORE_CLOSE = 'expected newline before close'
UNEXPECTED_NEWLINE = 'unexpected newline'
PARSING_ERROR = 'parsing error'
PuppetLint.new_check(:array_formatting) do
  def process_array
    # When we arrive here, the @manifest enumerator is "pointing" to
    # the opening LBRACK.  Start by iterating that to point to the
    # first element of the array:
    token = @manifest.next
    manifest_array = []
    len_of_nested_array = 0

    # Scan through the manifest until we encounter a RBRACK to close this array
    until [:RBRACK].include?(token.type) || token.nil?
      # If we encounter an LBRACK, we need to get our recursion on
      if [:LBRACK].include?(token.type)
        # Push the opening of the array as a placeholder unless the last token was a classref.
        # If it was, it was already pushed, so skip pushing.
        # We want to treat a classref and an array opening as a single token.
        manifest_array.push token unless %i[CLASSREF TYPE].include?(token.prev_code_token.type)
        # Recursively process this nested array.
        # Store return so we know if the child array was processed or not.
        len_of_nested_array = process_array
      # If we encounter a DQPRE, we need to scan ahead until we find the matching DQPOST
      elsif [:DQPRE].include?(token.type)
        # Tight loop until we find a DQPOST, or a Nil in case of an unterminated string
        until [:DQPOST].include?(token.type) || token.nil?
          # Advance.  We're still looking for that DQPOST
          token = @manifest.next
        end
        # Store the DQPOST
        manifest_array.push token
      # We're really only keeping track of array *items* here for the sake of the length calculation.
      # We're going to ignore everything that doesn't count as an *item*, like commas, comments, and whitespaces
      # We need to push NEWLINE(s) though because there might be too many
      elsif !%i[COMMA WHITESPACE INDENT].include?(token.type)
        manifest_array.push token
      end
      # Advance.  (We're still looking for that RBRACK)
      token = @manifest.next
    end

    # We've found the RBRACK, and built a copy of the array in the Puppet manifest in the
    # local variable manifest_array.  Before we start validating, let's try to return early.
    #
    # Check the number of objects (tokens + newlines) we found, including child arrays.
    # If it's one or less, we can leave and return that we're *not* formatting
    return manifest_array.length if (manifest_array.length < 2) && (len_of_nested_array < 2)

    # Array is 2 or more elements. Time to validate it.
    #
    # First element of the array should be a NEWLINE.
    prev_token_was_newline = false

    # Start iterating our rebuilt array
    manifest_array.each_index do |x|
      if !prev_token_was_newline && [:NEWLINE].include?(manifest_array[x].type)
        # We were expecting a newline, so yay
        prev_token_was_newline = true
      elsif prev_token_was_newline && ![:NEWLINE].include?(manifest_array[x].type)
        # We were expecting something other than a newline, so yay
        prev_token_was_newline = false
      elsif !prev_token_was_newline && ![:NEWLINE].include?(manifest_array[x].type)
        # We were expecting a newline. :-(
        notify :warning,
               message: EXPECTED_NEWLINE_BEFORE_ELEMENT,
               line: manifest_array[x].line,
               column: manifest_array[x].column,
               token: manifest_array[x]
        # prev_token_was_newline is already false, so this isn't required
        # prev_token_was_newline = false
      elsif prev_token_was_newline && [:NEWLINE].include?(manifest_array[x].type)
        # We got an extra newline. Let's fix that up while we're at it.
        notify :warning,
               message: UNEXPECTED_NEWLINE,
               line: manifest_array[x].line,
               column: manifest_array[x].column,
               token: manifest_array[x]
        # prev_token_was_newline is already true, so this isn't required
        # prev_token_was_newline = true
      else
        # Something has gone horribly wrong.  The truth table says we should never be here.
        notify :error,
               message: PARSING_ERROR,
               line: manifest_array[x].line,
               column: manifest_array[x].column
      end
      # Next manifest_array item
    end

    # All elements of the array are done.
    # If the last token (not including the RBRACK) wasn't a newline, throw a warning.
    # Since we previously used token to scan forward to the ],
    # token is already pointing to the offending ].
    if !prev_token_was_newline # rubocop:disable Style/NegatedIf "I find this clearer than `unless prev_token_was_newline`"
      notify :warning,
             message: EXPECTED_NEWLINE_BEFORE_CLOSE,
             line: token.line,
             column: token.column,
             token: token
    end

    # Return the length of the array so we can make assumptions about
    # whether we're formatting or not
    manifest_array.length
  end

  def check
    # Create an instance-scope enumerator to walk through the Puppet manifest
    @manifest = tokens.each
    loop do
      token = @manifest.next
      next unless [:LBRACK].include?(token.type)

      # Found an array!  Send if off for processing unless it's actually a Classref.
      # We can disregard the return, becuase this is a top level array, and has no 'parent' to affect
      process_array unless %i[CLASSREF TYPE].include?(token.prev_code_token.type)
    end
  end

  def fix(problem)
    token_index = tokens.find_index(problem[:token])
    case problem[:message]
    when PARSING_ERROR
      # Truth table says we should never get here
      raise PuppetLint::NoFix
    when UNEXPECTED_NEWLINE
      # If we delete the INDENT first, we change the index of the offending token,
      # so we need to do the token first, then the INDENT
      tokens.delete_at(token_index)
      # If the previous token is an Indent, we'll want to delete that too
      tokens.delete_at(token_index - 1) if [:INDENT].include?(problem[:token].prev_token.type)
    when EXPECTED_NEWLINE_BEFORE_ELEMENT, EXPECTED_NEWLINE_BEFORE_CLOSE
      # If the preceeding token is a whitespace, replace it with a newline instead
      # so that we don't end up with trailing whitespaces
      if %i[WHITESPACE INDENT].include?(problem[:token].prev_token.type)
        problem[:token].prev_token.type = :NEWLINE
      else
        # Create a new Puppet Token object and insert it into the Puppet manifest in the correct location.
        # The location specified in Token.new() apparently doesn't matter since we're manually inserting it
        # into the Tokens array at the proper position using token_index.
        tokens.insert(token_index, PuppetLint::Lexer::Token.new(:NEWLINE, '\n', 0, 0))
      end
    end
  end
end
