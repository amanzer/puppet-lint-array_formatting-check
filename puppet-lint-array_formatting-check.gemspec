# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name        = 'puppet-lint-array_formatting-check'
  spec.version     = '0.11.1'
  spec.homepage    = 'https://gitlab.com/amanzer/puppet-lint-array_formatting-check'
  spec.license     = 'MIT'
  spec.author      = 'Adam Manzer'
  spec.email       = 'amanzer@gmail.com'
  spec.files       = Dir[
    'README.md',
    'LICENSE',
    'lib/**/*',
    'spec/**/*',
  ]
  spec.summary     = 'A puppet-lint plugin to check array formatting.'
  spec.description = <<-DESC
    A puppet-lint plugin to check that arrays comform to the style guideline of having one element per line.
  DESC
  # Target at least Ruby 2.5, as that's what was in place in puppet-lint 3.1.0.
  spec.required_ruby_version = Gem::Requirement.new('>= 2.5')

  # This plugin uses the puppet-lint datatype :TYPE, which was introduced in puppet-lint 2.0.1
  # (https://github.com/rodjek/puppet-lint/pull/435)
  # So the hard floor requirement is 2.0.1.
  # I've chosen to require 3.1.0 here, as that's the first release under the new puppetlabs
  # management/namespace that also has the lowest Ruby requirement for maximum compatibility
  spec.add_dependency 'puppet-lint', '>= 3.1.0', '< 5'

  spec.metadata['rubygems_mfa_required'] = 'true'
end
