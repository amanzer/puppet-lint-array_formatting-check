# frozen_string_literal: true

require 'spec_helper'

describe 'array_formatting' do
  let(:msg) { 'expected newline before element' }
  let(:msg_end) { 'expected newline before close' }
  let(:msg_newline) { 'unexpected newline' }

  context 'with fix disabled' do
    context 'on code with an empty array' do
      let(:code) { '$services = []' }

      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end
    end
    context 'on code with a single-item array' do
      let(:code) { "$my_string = [ 'a' ]" }

      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end
    end
    context 'on code with a single-item array with a comma' do
      let(:code) { "$my_string = [ 'a', ]" }

      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end
    end
    context 'on code with a triple-nested array of weirdness' do
      let(:code) { '$my_array = [[[ 1 ]]]' }

      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end
    end
    context 'on code with a properly formatted array of numbers' do
      let(:code) do
        # Removes the 10 indentation spaces by replacing them with nothing
        # Thanks to Marius Rieder in check_strict_indent_spec
        <<-ARRAY.gsub(/^ {10}/, '')
          $my_numbers = [
            1,
            2,
            3,
          ]
        ARRAY
      end

      it 'should not detect any problems' do
        expect(problems).to have(0).problems
      end
    end
    context 'on code with a spaced single-line array' do
      let(:code) { "$my_strings = [ 'a', \"b\", 'c', 4, ]" }

      it 'should detect 5 problems' do
        expect(problems).to have(5).problems
      end

      it 'should create a warning' do
        # There should be a warning message, matching the
        # message above, flagged after the opening '['
        expect(problems).to contain_warning(msg).on_line(1).in_column(17)
        expect(problems).to contain_warning(msg).on_line(1).in_column(22)
        expect(problems).to contain_warning(msg).on_line(1).in_column(27)
        expect(problems).to contain_warning(msg).on_line(1).in_column(32)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(35)
      end
    end
    context 'on code with an unspaced single-line array' do
      let(:code) { "$my_strings = ['a','b','c',4]" }

      it 'should detect 5 problems' do
        expect(problems).to have(5).problems
      end

      it 'should create a warning' do
        # There should be a warning message, matching the
        # message above, flagged after the opening '['
        expect(problems).to contain_warning(msg).on_line(1).in_column(16)
        expect(problems).to contain_warning(msg).on_line(1).in_column(20)
        expect(problems).to contain_warning(msg).on_line(1).in_column(24)
        expect(problems).to contain_warning(msg).on_line(1).in_column(28)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(29)
      end
    end
    context 'on code with an inconsistently separated array' do
      let(:code) do
        <<-ARRAY.gsub(/^ {10}/, '')
          file{[
            '/etc', '/etc/app',
            '/opt', '/opt/app',
          ]:
            ensure => directory,
          }
        ARRAY
      end

      it 'should detect 2 problems' do
        expect(problems).to have(2).problems
      end

      it 'should create warnings' do
        expect(problems).to contain_warning(msg).on_line(2).in_column(11)
        expect(problems).to contain_warning(msg).on_line(3).in_column(11)
      end
    end
    context 'on code with an array with extra newlines' do
      let(:code) do
        # rubocop:disable Layout/TrailingWhitespace
        <<-ARRAY.gsub(/^ {10}/, '')
          file{[
            '/etc',
            
            '/etc/app',
            '/opt',
            
            '/opt/app',
          ]:
            ensure => directory,
          }
        ARRAY
        # rubocop:enable Layout/TrailingWhitespace
      end

      it 'should detect 2 problems' do
        expect(problems).to have(2).problems
      end

      it 'should create warnings' do
        expect(problems).to contain_warning(msg_newline).on_line(3).in_column(3)
        expect(problems).to contain_warning(msg_newline).on_line(6).in_column(3)
      end
    end
    context 'on code with a single child array of two elements' do
      let(:code) { '$my_array = [[ 1, 2 ]]' }

      it 'should detect 5 problems' do
        expect(problems).to have(5).problems
      end

      it 'should create warnings' do
        expect(problems).to contain_warning(msg).on_line(1).in_column(14)
        expect(problems).to contain_warning(msg).on_line(1).in_column(16)
        expect(problems).to contain_warning(msg).on_line(1).in_column(19)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(21)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(22)
      end
    end
    context 'on code with a two array-element array' do
      let(:code) { "$my_array = [ [1], ['a',] ]" }

      it 'should detect 3 problems' do
        expect(problems).to have(3).problems
      end

      it 'should create warnings' do
        expect(problems).to contain_warning(msg).on_line(1).in_column(15)
        expect(problems).to contain_warning(msg).on_line(1).in_column(20)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(27)
      end
    end
    context 'on code with an array with a two-element array nested second' do
      let(:code) { "$my_array = [ [1], ['a', 'b'] ]" }

      it 'should detect 6 problems' do
        expect(problems).to have(6).problems
      end

      it 'should create warnings' do
        expect(problems).to contain_warning(msg).on_line(1).in_column(15)
        expect(problems).to contain_warning(msg).on_line(1).in_column(20)
        expect(problems).to contain_warning(msg).on_line(1).in_column(21)
        expect(problems).to contain_warning(msg).on_line(1).in_column(26)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(29)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(31)
      end
    end
    context 'on code with an array with a two-element array nested first' do
      let(:code) { "$my_array = [ [1,2], ['a',] ]" }

      it 'should detect 6 problems' do
        expect(problems).to have(6).problems
      end

      it 'should create warnings' do
        expect(problems).to contain_warning(msg).on_line(1).in_column(15)
        expect(problems).to contain_warning(msg).on_line(1).in_column(16)
        expect(problems).to contain_warning(msg).on_line(1).in_column(18)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(19)
        expect(problems).to contain_warning(msg).on_line(1).in_column(22)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(29)
      end
    end
    context 'on code with a single-value classref' do
      let(:code) { "subscribe => File['/etc/fstab']" }

      it 'should detect no problems' do
        expect(problems).to have(0).problems
      end
    end
    context 'on code with a single-value classref in an array' do
      let(:code) { "subscribe => [File['/etc/fstab'],]" }

      it 'should detect no problems' do
        expect(problems).to have(0).problems
      end
    end
    context 'on code with a multi-classref array' do
      let(:code) { "subscribe => [File['/etc/fstab'], Service['sshd'],]" }

      it 'should detect three problems' do
        expect(problems).to have(3).problems
      end

      it 'should create warnings' do
        expect(problems).to contain_warning(msg).on_line(1).in_column(15)
        expect(problems).to contain_warning(msg).on_line(1).in_column(35)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(51)
      end
    end
    context 'on code with a nested classref array' do
      let(:code) { "subscribe => [ File['/etc/fstab','/etc/auto.master',] ]," }

      it 'should detect 5 problems' do
        expect(problems).to have(5).problems
      end

      it 'should create warnings' do
        expect(problems).to contain_warning(msg).on_line(1).in_column(16)
        expect(problems).to contain_warning(msg).on_line(1).in_column(21)
        expect(problems).to contain_warning(msg).on_line(1).in_column(34)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(53)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(55)
      end
    end
    context 'on code with an array of  multiple classrefs' do
      let(:code) { "subscribe => [File['/etc/fstab','/etc/auto.master'], Service['sshd'],]" }

      it 'should detect six problems' do
        expect(problems).to have(6).problems
      end

      it 'should create warnings' do
        expect(problems).to contain_warning(msg).on_line(1).in_column(15)
        expect(problems).to contain_warning(msg).on_line(1).in_column(20)
        expect(problems).to contain_warning(msg).on_line(1).in_column(33)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(51)
        expect(problems).to contain_warning(msg).on_line(1).in_column(54)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(70)
      end
    end
    context 'on code with an array of 2 two-value classrefs' do
      let(:code) { "subscribe => [File['/etc/fstab','/etc/auto.master'], Service['sshd',$ntp::service],]" }

      it 'should detect six problems' do
        expect(problems).to have(9).problems
      end

      it 'should create warnings' do
        expect(problems).to contain_warning(msg).on_line(1).in_column(15)
        expect(problems).to contain_warning(msg).on_line(1).in_column(20)
        expect(problems).to contain_warning(msg).on_line(1).in_column(33)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(51)
        expect(problems).to contain_warning(msg).on_line(1).in_column(54)
        expect(problems).to contain_warning(msg).on_line(1).in_column(62)
        expect(problems).to contain_warning(msg).on_line(1).in_column(69)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(82)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(84)
      end
    end
    context 'on code with an Enum' do
      let(:code) { 'Enum[3.3, 5] $voltage' }

      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end
    end
    context 'on code with six different arrays' do
      let(:code) do
        <<-CODE.gsub(/^ {10}/, '')
          $file_list1 = ['/etc/file1', '/etc/file2']
          $file_list2 = ['/etc/file3',]
          $file_list3 = ['/etc/file4', '/etc/file5',]
          file{[ $file_list1,
            $file_list2, $file_list3,
          ]:
            ensure    => directory,
            subscribe => [File[$file_list1, $file_list2]]
            notify    => [File[$file_list2, $file_list3]]
          }
        CODE
      end

      it 'should detect 18 problems' do
        expect(problems).to have(18).problems
      end

      it 'should create warnings' do
        expect(problems).to contain_warning(msg).on_line(1).in_column(16)
        expect(problems).to contain_warning(msg).on_line(1).in_column(30)
        expect(problems).to contain_warning(msg_end).on_line(1).in_column(42)
        expect(problems).to contain_warning(msg).on_line(3).in_column(16)
        expect(problems).to contain_warning(msg).on_line(3).in_column(30)
        expect(problems).to contain_warning(msg_end).on_line(3).in_column(43)
        expect(problems).to contain_warning(msg).on_line(4).in_column(8)
        expect(problems).to contain_warning(msg).on_line(5).in_column(16)
        expect(problems).to contain_warning(msg).on_line(8).in_column(17)
        expect(problems).to contain_warning(msg).on_line(8).in_column(22)
        expect(problems).to contain_warning(msg).on_line(8).in_column(35)
        expect(problems).to contain_warning(msg_end).on_line(8).in_column(46)
        expect(problems).to contain_warning(msg_end).on_line(8).in_column(47)
        expect(problems).to contain_warning(msg).on_line(9).in_column(17)
        expect(problems).to contain_warning(msg).on_line(9).in_column(22)
        expect(problems).to contain_warning(msg).on_line(9).in_column(35)
        expect(problems).to contain_warning(msg_end).on_line(9).in_column(46)
        expect(problems).to contain_warning(msg_end).on_line(9).in_column(47)
      end
    end
    context 'on a complete code example with no issues' do
      let(:code) do
        <<-CODE.gsub(/^ {10}/, '')
          class testing(
            Enum['present', 'absent'] $package_ensure,
            Array[String] $keys,
            Boolean $keys_enable,
            Optional[Integer[0]] $panic,
            Optional[Array[String]] $pool,
            Variant[Boolean, Integer[0,1]] $tos_cohort,
          ) {
            if $facts['is_virtual'] {
              $_tinker = pick($tinker, true)
            } else {
              $tinker = pick($tinker, false)
            }
            Class['::ntp::install']
            -> Class['::ntp::config']
            ~> Class['::ntp::service']
          }
        CODE
      end

      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end
    end
    context 'on a complete code example with one bad array' do
      let(:code) do
        <<-CODE.gsub(/^ {10}/, '')
          class testing(
            Enum['present', 'absent'] $package_ensure,
            Array[String] $keys,
            Boolean $keys_enable,
            Optional[Integer[0]] $panic,
            Optional[Array[String]] $pool,
            Variant[Boolean, Integer[0,1]] $tos_cohort,
          ) {
            if $facts['is_virtual'] {
              $_tinker = pick($tinker, true)
            } else {
              $tinker = pick($tinker, false)
            }
            $keys_parameters = [$keys, $keys_enable]
            Class['::ntp::install']
            -> Class['::ntp::config']
            ~> Class['::ntp::service']
          }
        CODE
      end

      it 'should detect 3 problems' do
        expect(problems).to have(3).problems
      end

      it 'should create warnings' do
        expect(problems).to contain_warning(msg).on_line(14).in_column(23)
        expect(problems).to contain_warning(msg).on_line(14).in_column(30)
        expect(problems).to contain_warning(msg_end).on_line(14).in_column(42)
      end
    end
    context 'on a string with a properly formatted multidimensional hash (Issue #18)' do
      let(:code) do
        <<-CODE.gsub(/^ {10}/, '')
          Apt::Source["yubihsm-${facts['os']['distro']['codename']}"],
        CODE
      end
      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end
    end
    context 'on a code snippet with a properly formatted multidimensional hash (Issue #18)' do
      let(:code) do
        <<-CODE.gsub(/^ {10}/, '')
        package { $packages:
          ensure  => $version,
          require => [
            Class['apt::update'],
            Apt::Source["yubihsm-${facts['os']['distro']['codename']}"],
          ],
        }
        CODE
      end
      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end
    end
    context 'on a string with a properly formatted variable (Issue #19)' do
      let(:code) do
        <<-CODE.gsub(/^ {10}/, '')
        file { [
            "${root_dir}/index.txt",
            "${root_dir}/index.txt.attr"
          ]:
            ensure => file,
            owner => 0,
            group => 0,
            mode => '0400',
        }
        CODE
      end
      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end
    end
    context 'on an array with comments after some values (Issue #20)' do
      let(:code) do
        <<-CODE.gsub(/^ {10}/, '')
        proxy_set_header   => [
          'Host $http_host:8043',
          'X-Real-IP $remote_addr',
          'X-Forwarded-For $proxy_add_x_forwarded_for',
          'Cookie $http_cookie',
          'X-Forwarded-Proto $scheme',
          'Upgrade $http_upgrade',  #websockets
          'Connection "Upgrade"',  #websockets
        ],
        CODE
      end
      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end
    end
  end
  context 'with fix enabled' do
    before do
      PuppetLint.configuration.fix = true
    end

    after do
      PuppetLint.configuration.fix = false
    end
    context 'on code with an empty array' do
      let(:code) { '$services = []' }

      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end

      it 'should not modify the manifest' do
        expect(manifest).to eq(code)
      end
    end
    context 'on code with a single-item array' do
      let(:code) { "$my_string = [ 'a' ]" }

      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end

      it 'should not modify the manifest' do
        expect(manifest).to eq(code)
      end
    end
    context 'on code with a single-item array with a comma' do
      let(:code) { "$my_string = [ 'a', ]" }

      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end

      it 'should not modify the manifest' do
        expect(manifest).to eq(code)
      end
    end
    context 'on code with a triple-nested array of weirdness' do
      let(:code) { '$my_array = [[[ 1 ]]]' }

      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end

      it 'should not modify the manifest' do
        expect(manifest).to eq(code)
      end
    end
    context 'on code with a properly formatted array of numbers' do
      let(:code) do
        # Removes the 10 indentation spaces by replacing them with nothing
        # Thanks to Marius Rieder in check_strict_indent_spec
        <<-ARRAY.gsub(/^ {10}/, '')
          $my_numbers = [
            1,
            2,
            3,
          ]
        ARRAY
      end

      it 'should not detect any problems' do
        expect(problems).to have(0).problems
      end

      it 'should not modify the manifest' do
        expect(manifest).to eq(code)
      end
    end
    context 'on code with a spaced single-line array' do
      let(:code) { "$my_strings = [ 'a', \"b\", 'c', 4, ]" }

      it 'should detect 5 problems' do
        expect(problems).to have(5).problems
      end

      it 'should generate fixes' do
        # There should be a warning message, matching the
        # message above, flagged after the opening '['
        expect(problems).to contain_fixed(msg).on_line(1).in_column(17)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(22)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(27)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(32)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(35)
      end

      it 'should properly format the array' do
        expect(manifest).to eq("$my_strings = [\n'a',\n\"b\",\n'c',\n4,\n]")
      end
    end
    context 'on code with an unspaced single-line array' do
      let(:code) { "$my_strings = ['a','b','c',4]" }

      it 'should detect 5 problems' do
        expect(problems).to have(5).problems
      end

      it 'should generate fixes' do
        # There should be a warning message, matching the
        # message above, flagged after the opening '['
        expect(problems).to contain_fixed(msg).on_line(1).in_column(16)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(20)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(24)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(28)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(29)
      end

      it 'should properly format the array' do
        expect(manifest).to eq("$my_strings = [\n'a',\n'b',\n'c',\n4\n]")
      end
    end
    context 'on code with an inconsistently separated array' do
      let(:code) do
        <<-ARRAY.gsub(/^ {10}/, '')
          file{[
            '/etc', '/etc/app',
            '/opt', '/opt/app',
          ]:
            ensure => directory,
          }
        ARRAY
      end

      it 'should detect 2 problems' do
        expect(problems).to have(2).problems
      end

      it 'should create fixes ' do
        expect(problems).to contain_fixed(msg).on_line(2).in_column(11)
        expect(problems).to contain_fixed(msg).on_line(3).in_column(11)
      end

      it 'should properly format the array' do
        expect(manifest).to eq(
          # This is the expected code. This plugin doesn't do indentation, so it's expected
          # that the moved lines will be "bad" indentation.  Hopefully the indentation fixer
          # can run after this plugin.
          <<-ARRAY.gsub(/^ {12}/, '')
            file{[
              '/etc',
            '/etc/app',
              '/opt',
            '/opt/app',
            ]:
              ensure => directory,
            }
          ARRAY
        )
      end
    end
    context 'on code with an array with extra newlines' do
      let(:code) do
        # rubocop:disable Layout/TrailingWhitespace
        <<-ARRAY.gsub(/^ {10}/, '')
          file{[
            '/etc',
            
            '/etc/app',
            '/opt',
            
            '/opt/app',
          ]:
            ensure => directory,
          }
        ARRAY
        # rubocop:enable Layout/TrailingWhitespace
      end

      it 'should detect 2 problems' do
        expect(problems).to have(2).problems
      end

      it 'should create fixes' do
        expect(problems).to contain_fixed(msg_newline).on_line(3).in_column(3)
        expect(problems).to contain_fixed(msg_newline).on_line(6).in_column(3)
      end

      it 'should properly format the array' do
        expect(manifest).to eq(
          <<-ARRAY.gsub(/^ {12}/, '')
            file{[
              '/etc',
              '/etc/app',
              '/opt',
              '/opt/app',
            ]:
              ensure => directory,
            }
          ARRAY
        )
      end
    end
    context 'on code with a single child array of two elements' do
      let(:code) { '$my_array = [[ 1, 2 ]]' }

      it 'should detect 5 problems' do
        expect(problems).to have(5).problems
      end

      it 'should create fixes' do
        expect(problems).to contain_fixed(msg).on_line(1).in_column(14)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(16)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(19)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(21)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(22)
      end

      it 'should properly format the array' do
        expect(manifest).to eq(
          # Again, this plugin doesn't indent
          <<-ARRAY.chomp.gsub(/^ {12}/, '')
            $my_array = [
            [
            1,
            2
            ]
            ]
          ARRAY
        )
      end
    end
    context 'on code with a two array-element array' do
      let(:code) { "$my_array = [ [1], ['a',] ]" }

      it 'should detect 3 problems' do
        expect(problems).to have(3).problems
      end

      it 'should create fixes' do
        expect(problems).to contain_fixed(msg).on_line(1).in_column(15)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(20)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(27)
      end

      it 'should properly format the array' do
        expect(manifest).to eq(
          # Again, this plugin doesn't indent
          <<-ARRAY.chomp.gsub(/^ {12}/, '')
            $my_array = [
            [1],
            ['a',]
            ]
          ARRAY
        )
      end
    end
    context 'on code with an array with a two-element array nested second' do
      let(:code) { "$my_array = [ [1], ['a', 'b'] ]" }

      it 'should detect 6 problems' do
        expect(problems).to have(6).problems
      end

      it 'should create fixes' do
        expect(problems).to contain_fixed(msg).on_line(1).in_column(15)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(20)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(21)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(26)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(29)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(31)
      end

      it 'should properly format the array' do
        expect(manifest).to eq(
          # Again, this plugin doesn't indent
          <<-ARRAY.chomp.gsub(/^ {12}/, '')
            $my_array = [
            [1],
            [
            'a',
            'b'
            ]
            ]
          ARRAY
        )
      end
    end
    context 'on code with an array with a two-element array nested first' do
      let(:code) { "$my_array = [ [1,2], ['a',] ]" }

      it 'should detect 6 problems' do
        expect(problems).to have(6).problems
      end

      it 'should create fixes' do
        expect(problems).to contain_fixed(msg).on_line(1).in_column(15)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(16)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(18)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(19)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(22)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(29)
      end

      it 'should properly format the array' do
        expect(manifest).to eq(
          # Again, this plugin doesn't indent
          <<-ARRAY.chomp.gsub(/^ {12}/, '')
            $my_array = [
            [
            1,
            2
            ],
            ['a',]
            ]
          ARRAY
        )
      end
    end
    context 'on code with a single-value classref' do
      let(:code) { "subscribe => File['/etc/fstab']" }

      it 'should detect no problems' do
        expect(problems).to have(0).problems
      end

      it 'should not modify the manifest' do
        expect(manifest).to eq(code)
      end
    end
    context 'on code with a single-value classref in an array' do
      let(:code) { "subscribe => [File['/etc/fstab'],]" }

      it 'should detect no problems' do
        expect(problems).to have(0).problems
      end

      it 'should not modify the manifest' do
        expect(manifest).to eq(code)
      end
    end
    context 'on code with a multi-classref array' do
      let(:code) { "subscribe => [File['/etc/fstab'], Service['sshd'],]" }

      it 'should detect three problems' do
        expect(problems).to have(3).problems
      end

      it 'should create fixes' do
        expect(problems).to contain_fixed(msg).on_line(1).in_column(15)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(35)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(51)
      end

      it 'should properly format the array' do
        expect(manifest).to eq(
          <<-ARRAY.chomp.gsub(/^ {12}/, '')
            subscribe => [
            File['/etc/fstab'],
            Service['sshd'],
            ]
          ARRAY
        )
      end
    end
    context 'on code with a nested classref array' do
      let(:code) { "subscribe => [ File['/etc/fstab','/etc/auto.master',] ]," }

      it 'should detect 5 problems' do
        expect(problems).to have(5).problems
      end

      it 'should create fixes' do
        expect(problems).to contain_fixed(msg).on_line(1).in_column(16)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(21)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(34)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(53)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(55)
      end

      it 'should properly format the array' do
        expect(manifest).to eq(
          <<-ARRAY.chomp.gsub(/^ {12}/, '')
            subscribe => [
            File[
            '/etc/fstab',
            '/etc/auto.master',
            ]
            ],
          ARRAY
        )
      end
    end
    context 'on code with an array of  multiple classrefs' do
      let(:code) { "subscribe => [File['/etc/fstab','/etc/auto.master'], Service['sshd'],]" }

      it 'should detect six problems' do
        expect(problems).to have(6).problems
      end

      it 'should create fixes' do
        expect(problems).to contain_fixed(msg).on_line(1).in_column(15)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(20)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(33)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(51)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(54)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(70)
      end

      it 'should properly format the array' do
        expect(manifest).to eq(
          <<-ARRAY.chomp.gsub(/^ {12}/, '')
            subscribe => [
            File[
            '/etc/fstab',
            '/etc/auto.master'
            ],
            Service['sshd'],
            ]
          ARRAY
        )
      end
    end
    context 'on code with an array of 2 two-value classrefs' do
      let(:code) { "subscribe => [File['/etc/fstab','/etc/auto.master'], Service['sshd',$ntp::service],]" }

      it 'should detect six problems' do
        expect(problems).to have(9).problems
      end

      it 'should create fixes' do
        expect(problems).to contain_fixed(msg).on_line(1).in_column(15)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(20)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(33)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(51)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(54)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(62)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(69)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(82)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(84)
      end

      it 'should properly format the array' do
        expect(manifest).to eq(
          <<-ARRAY.chomp.gsub(/^ {12}/, '')
            subscribe => [
            File[
            '/etc/fstab',
            '/etc/auto.master'
            ],
            Service[
            'sshd',
            $ntp::service
            ],
            ]
          ARRAY
        )
      end
    end
    context 'on code fragment with an Enum' do
      let(:code) { 'Enum[3.3, 5] $voltage' }

      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end

      it 'should not modify the manifest' do
        expect(manifest).to eq(code)
      end
    end
    context 'on code with six different arrays' do
      let(:code) do
        <<-CODE.gsub(/^ {10}/, '')
          $file_list1 = ['/etc/file1', '/etc/file2']
          $file_list2 = ['/etc/file3',]
          $file_list3 = ['/etc/file4', '/etc/file5',]
          file{[ $file_list1,
            $file_list2, $file_list3,
          ]:
            ensure    => directory,
            subscribe => [File[$file_list1, $file_list2]]
            notify    => [File[$file_list2, $file_list3]]
          }
        CODE
      end

      it 'should detect 18 problems' do
        expect(problems).to have(18).problems
      end

      it 'should create fixes' do
        expect(problems).to contain_fixed(msg).on_line(1).in_column(16)
        expect(problems).to contain_fixed(msg).on_line(1).in_column(30)
        expect(problems).to contain_fixed(msg_end).on_line(1).in_column(42)
        expect(problems).to contain_fixed(msg).on_line(3).in_column(16)
        expect(problems).to contain_fixed(msg).on_line(3).in_column(30)
        expect(problems).to contain_fixed(msg_end).on_line(3).in_column(43)
        expect(problems).to contain_fixed(msg).on_line(4).in_column(8)
        expect(problems).to contain_fixed(msg).on_line(5).in_column(16)
        expect(problems).to contain_fixed(msg).on_line(8).in_column(17)
        expect(problems).to contain_fixed(msg).on_line(8).in_column(22)
        expect(problems).to contain_fixed(msg).on_line(8).in_column(35)
        expect(problems).to contain_fixed(msg_end).on_line(8).in_column(46)
        expect(problems).to contain_fixed(msg_end).on_line(8).in_column(47)
        expect(problems).to contain_fixed(msg).on_line(9).in_column(17)
        expect(problems).to contain_fixed(msg).on_line(9).in_column(22)
        expect(problems).to contain_fixed(msg).on_line(9).in_column(35)
        expect(problems).to contain_fixed(msg_end).on_line(9).in_column(46)
        expect(problems).to contain_fixed(msg_end).on_line(9).in_column(47)
      end

      it 'should properly format the code' do
        # Don't forget this plugin doesn't do indentation, and the $file_list2 use below was
        # already properly indented in the original code
        expect(manifest).to eq(
          <<-CODE.gsub(/^ {12}/, '')
            $file_list1 = [
            '/etc/file1',
            '/etc/file2'
            ]
            $file_list2 = ['/etc/file3',]
            $file_list3 = [
            '/etc/file4',
            '/etc/file5',
            ]
            file{[
            $file_list1,
              $file_list2,
            $file_list3,
            ]:
              ensure    => directory,
              subscribe => [
            File[
            $file_list1,
            $file_list2
            ]
            ]
              notify    => [
            File[
            $file_list2,
            $file_list3
            ]
            ]
            }
          CODE
        )
      end
    end
    context 'on a complete code example with no issues' do
      let(:code) do
        <<-CODE.gsub(/^ {10}/, '')
          class testing(
            Enum['present', 'absent'] $package_ensure,
            Array[String] $keys,
            Boolean $keys_enable,
            Optional[Integer[0]] $panic,
            Optional[Array[String]] $pool,
            Variant[Boolean, Integer[0,1]] $tos_cohort,
          ) {
            if $facts['is_virtual'] {
              $_tinker = pick($tinker, true)
            } else {
              $tinker = pick($tinker, false)
            }
            Class['::ntp::install']
            -> Class['::ntp::config']
            ~> Class['::ntp::service']
          }
        CODE
      end

      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end

      it 'should not modify the manifest' do
        expect(manifest).to eq(code)
      end
    end
    context 'on a complete code example with one bad array' do
      let(:code) do
        <<-CODE.gsub(/^ {10}/, '')
          class testing(
            Enum['present', 'absent'] $package_ensure,
            Array[String] $keys,
            Boolean $keys_enable,
            Optional[Integer[0]] $panic,
            Optional[Array[String]] $pool,
            Variant[Boolean, Integer[0,1]] $tos_cohort,
          ) {
            if $facts['is_virtual'] {
              $_tinker = pick($tinker, true)
            } else {
              $tinker = pick($tinker, false)
            }
            $keys_parameters = [$keys, $keys_enable]
            Class['::ntp::install']
            -> Class['::ntp::config']
            ~> Class['::ntp::service']
          }
        CODE
      end

      it 'should detect 3 problems' do
        expect(problems).to have(3).problems
      end

      it 'should create fixes' do
        expect(problems).to contain_fixed(msg).on_line(14).in_column(23)
        expect(problems).to contain_fixed(msg).on_line(14).in_column(30)
        expect(problems).to contain_fixed(msg_end).on_line(14).in_column(42)
      end

      it 'should properly format the manifest' do
        expect(manifest).to eq(
          <<-CODE.gsub(/^ {12}/, '')
            class testing(
              Enum['present', 'absent'] $package_ensure,
              Array[String] $keys,
              Boolean $keys_enable,
              Optional[Integer[0]] $panic,
              Optional[Array[String]] $pool,
              Variant[Boolean, Integer[0,1]] $tos_cohort,
            ) {
              if $facts['is_virtual'] {
                $_tinker = pick($tinker, true)
              } else {
                $tinker = pick($tinker, false)
              }
              $keys_parameters = [
            $keys,
            $keys_enable
            ]
              Class['::ntp::install']
              -> Class['::ntp::config']
              ~> Class['::ntp::service']
            }
          CODE
        )
      end
    end
    context 'on a string with a properly formatted multidimensional hash (Issue #18)' do
      let(:code) do
        <<-CODE.gsub(/^ {10}/, '')
          Apt::Source["yubihsm-${facts['os']['distro']['codename']}"],
        CODE
      end
      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end

      it 'should not modify the manifest' do
        expect(manifest).to eq(code)
      end
    end
    context 'on a code snippet with a properly formatted multidimensional hash (Issue #18)' do
      let(:code) do
        <<-CODE.gsub(/^ {10}/, '')
        package { $packages:
          ensure  => $version,
          require => [
            Class['apt::update'],
            Apt::Source["yubihsm-${facts['os']['distro']['codename']}"],
          ],
        }
        CODE
      end
      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end

      it 'should not modify the manifest' do
        expect(manifest).to eq(code)
      end
    end
    context 'on a string with a properly formatted variable (Issue #19)' do
      let(:code) do
        <<-CODE.gsub(/^ {10}/, '')
        file { [
            "${root_dir}/index.txt",
            "${root_dir}/index.txt.attr"
          ]:
            ensure => file,
            owner => 0,
            group => 0,
            mode => '0400',
        }
        CODE
      end
      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end

      it 'should not modify the manifest' do
        expect(manifest).to eq(code)
      end
    end
    context 'on an array with comments after some values (Issue #20)' do
      let(:code) do
        <<-CODE.gsub(/^ {10}/, '')
        proxy_set_header   => [
          'Host $http_host:8043',
          'X-Real-IP $remote_addr',
          'X-Forwarded-For $proxy_add_x_forwarded_for',
          'Cookie $http_cookie',
          'X-Forwarded-Proto $scheme',
          'Upgrade $http_upgrade',  #websockets
          'Connection "Upgrade"',  #websockets
        ],
        CODE
      end
      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end

      it 'should not modify the manifest' do
        expect(manifest).to eq(code)
      end
    end
  end
end
