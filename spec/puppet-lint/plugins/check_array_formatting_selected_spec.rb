# frozen_string_literal: true

require 'spec_helper'

describe 'array_formatting' do
  let(:msg) { 'expected newline before element' }
  let(:msg_end) { 'expected newline before close' }
  let(:msg_newline) { 'unexpected newline' }

  context 'with fix disabled' do
    context 'on code with an empty array' do
      let(:code) { '$services = []' }

      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end
    end
  end
  context 'with fix enabled' do
    before do
      PuppetLint.configuration.fix = true
    end

    after do
      PuppetLint.configuration.fix = false
    end
    context 'on code with an empty array' do
      let(:code) { '$services = []' }

      it 'should detect no problem' do
        expect(problems).to have(0).problems
      end

      it 'should not modify the manifest' do
        expect(manifest).to eq(code)
      end
    end
  end
end
