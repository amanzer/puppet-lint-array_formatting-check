# frozen_string_literal: true

require 'simplecov'

SimpleCov.start do
  add_filter '/bin/'
  add_filter '/db/'
  add_filter '/spec/' # for rspec
  add_filter '/test/' # for minitest
end

require 'puppet-lint'

PuppetLint::Plugins.load_spec_helper
