# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.11.1] - 2023-05-11
Further testing of 0.11.0 has shown that comment processing is bad and is now breaking arrays.
### Changed
- Revert [#20](https://gitlab.com/amanzer/puppet-lint-array_formatting-check/-/issues/20)
## [0.11.0] - 2023-05-11
This release goes out to @JvGinkel on GitLab, the only person in the world who has asked me about this project.  Thank you Jethro, you got me back into this puppet plugin. :smile:
### Added
- Added gem dependencies, and a launch.json for VSCode to enable debugging of the plugin
### Changed
- Updated array parsing so that double-quotes-strings-with-variables are treated as a single token for the purposes of counting how many items are in an array [#19](https://gitlab.com/amanzer/puppet-lint-array_formatting-check/-/issues/19)
- Updated handling of comments: [#20](https://gitlab.com/amanzer/puppet-lint-array_formatting-check/-/issues/20)
## [0.10.0] - 2023-05-02
This release is mostly to restart my development environment, and update the dependencies and code formatting to modern standards.  There are no code changes in this release.
### Added
Nothing added

### Changed
- Update gemspec file with new dependencies
- Update required puppet-lint version to 3.1.0
## [0.9.0] - 2019-03-14
### Added
- Add support for puppet-lint gem v2.x.  (Tested with puppet-lint 2.3.6)
- Improve handling things that look like arrays, but can't be formatted, by adding variable typing (ex: `Variant[Boolean, Integer[0,1]] $tos_cohort`). ([#12](https://gitlab.com/amanzer/puppet-lint-array_formatting-check/issues/12))
- Handle extra newlines by deleting them and their preceeding indent. ([#7](https://gitlab.com/amanzer/puppet-lint-array_formatting-check/issues/7))
- Add handling of unexpected errors
- Add simplecov tests to spec files.
- Add CHANGELOG

### Changed
- Update upstream gem compatibility to puppet-lint >=1.0, <3.0.

## [0.7.0] - 2019-03-06
_No Public Release_
### Added
- Handle nested arrays by refactoring plugin to be recursive. ([#5](https://gitlab.com/amanzer/puppet-lint-array_formatting-check/issues/5))
- Ensure that arrays that are partially spaced are handled correctly. ([#6](https://gitlab.com/amanzer/puppet-lint-array_formatting-check/issues/6))
- Handle Enums, which look like arrays, but can't be formatted (ex: `Enum['present', 'absent'] $package_ensure`). ([#10](https://gitlab.com/amanzer/puppet-lint-array_formatting-check/issues/10))
- Handle classrefs by ensuring that the array doesn't get separated from the classref. ([#11](https://gitlab.com/amanzer/puppet-lint-array_formatting-check/issues/11))

## [0.5.0] - 2018-12-27
_No Public Release_
### Added
- Add fixing of simple (non-nested) arrays. ([#4](https://gitlab.com/amanzer/puppet-lint-array_formatting-check/issues/4))

   Note that this gem only inserts newlines in the right places, and _does not_ indent.  There are other indentation plugins for that.

## [0.3.0] - 2018-11-04
_No Public Release_
### Added
- Initial private relase.
- Finds simple arrays, parses them, and correctly throws warnings in the right places.