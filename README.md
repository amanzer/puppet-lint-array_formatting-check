# puppet-lint array formatting check

[![Gem Version](https://badge.fury.io/rb/puppet-lint-array_formatting-check.svg)](https://badge.fury.io/rb/puppet-lint-array_formatting-check)

A new check for puppet-lint that ensures array formatting conforms to the [Puppet Style Guide](https://puppet.com/docs/puppet/6.0/style_guide.html#arrays-and-hashes) on arrays; namely that each element should be on a separate line.

It will take an array formatted as

```puppet
$packages = [ 'vim', 'sl', 'bind-utils', ]
```

and ensure that it's formatted as

```puppet
$packages = [
  'vim',
  'sl',
  'bind-utils',
]
```

In the name of readability, it'll ignore arrays with one element.

```puppet
$my_short_package_list = [ 'emacs', ] 
```

## Installation

To use this plugin, add the following line to your Gemfile

```ruby
gem 'puppet-lint-array_formatting-check'
```

and then run `bundle install`.

You can also install it at a user level using

```bash
gem install --user-install puppet-lint-array_formatting-check
```

or at a system level using

```bash
sudo gem install puppet-lint-array_formatting-check
```

## Usage

This plugin provides a new check to `puppet-lint`.

### array-formatting

**--fix support: Yes**

This check will raise warnings for every element in an array that should be on its own line

```
WARNING: expected newline before element on line 3
```

It will also raise warnings on extra (unnecessary) newlines in arrays

```
WARNING: unexpected newline on line 4
```

### Author

[Adam Manzer](https://gitlab.com/amanzer)